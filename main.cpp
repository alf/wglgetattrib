#include <windows.h>
#include <iostream>
#include <format>
#include <string>

#include "glad_wgl.h"

static HWND create_dummy_window()
{
    WNDCLASSEXW wndclass = {};

    wndclass.cbSize = sizeof(wndclass);
    wndclass.lpszClassName = L"wglchoose";
    wndclass.lpfnWndProc = DefWindowProcW;
    wndclass.style = CS_OWNDC;
    wndclass.hInstance = GetModuleHandle(NULL);
    if (!RegisterClassExW(&wndclass))
    {
        std::cerr << "Failed RegisterClassW" << std::endl;
        return NULL;
    }

    HWND hwnd = CreateWindowExW(0, wndclass.lpszClassName, NULL, WS_OVERLAPPED, 0, 0, 100, 100, NULL, NULL, NULL, NULL);
    if (!hwnd)
    {
        std::cerr << "Failed CreateWindowExW" << std::endl;
        return NULL;
    }

    return hwnd;
}

static void set_dummy_pixel_format(HDC hdc)
{
    PIXELFORMATDESCRIPTOR pfd = {};
    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_DRAW_TO_WINDOW;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.iLayerType = PFD_MAIN_PLANE;

    int fmt = ChoosePixelFormat(hdc, &pfd);
    if (!SetPixelFormat(hdc, fmt, &pfd))
        std::cerr << "Failed to set pixel format " << fmt << std::endl;
}

static void get_attrib(HDC hdc, int op)
{
    int num_formats_attrib = WGL_NUMBER_PIXEL_FORMATS_ARB;
    int invalid_attrib0[2] = { 0, WGL_NUMBER_PIXEL_FORMATS_ARB };
    int invalid_attrib1[2] = { WGL_NUMBER_PIXEL_FORMATS_ARB, 0 };
    int valid_attrib[2] = { WGL_RED_BITS_ARB, WGL_NUMBER_PIXEL_FORMATS_ARB };
    int valid_attrib_no_layer[2] = { WGL_SWAP_METHOD_ARB, WGL_NUMBER_OVERLAYS_ARB };
    int values[2] = { 0 };
    std::string result;
    BOOL ret;
    DWORD err;

#define TRY_ATTRIB(call) \
    std::cout << #call " => "; \
    std::cout.flush(); \
    values[0] = values[1] = -123456; \
    SetLastError(0); \
    ret = call; \
    err = GetLastError(); \
    std::cout << std::to_string(ret) << (!ret ? std::format(" 0x{:x}", err) : "") \
              << " v[" << values[0] << ", " << values[1] << "]" << std::endl; \
    break;

   switch (op)
   {
   case 0: TRY_ATTRIB(wglGetPixelFormatAttribivARB(hdc, 0, 0, 0, NULL, NULL));
   case 1: TRY_ATTRIB(wglGetPixelFormatAttribivARB(hdc, 1, 0, 0, NULL, NULL));
   case 2: TRY_ATTRIB(wglGetPixelFormatAttribivARB(hdc, 0, 0, 1, NULL, NULL));
   case 3: TRY_ATTRIB(wglGetPixelFormatAttribivARB(hdc, 1, 0, 1, NULL, NULL));
   case 4: TRY_ATTRIB(wglGetPixelFormatAttribivARB(hdc, -1, 0, 1, &num_formats_attrib, values));
   case 5: TRY_ATTRIB(wglGetPixelFormatAttribivARB(hdc, 1, 0, 2, invalid_attrib0, values));
   case 6: TRY_ATTRIB(wglGetPixelFormatAttribivARB(hdc, 1, 0, 2, invalid_attrib1, values));
   case 7: TRY_ATTRIB(wglGetPixelFormatAttribivARB(hdc, 1, 0, 2, invalid_attrib0, NULL));
   case 8: TRY_ATTRIB(wglGetPixelFormatAttribivARB(hdc, 1, 0, 2, invalid_attrib1, NULL));
   case 9: TRY_ATTRIB(wglGetPixelFormatAttribivARB(hdc, 3000, 0, 2, valid_attrib, NULL));
   case 10: TRY_ATTRIB(wglGetPixelFormatAttribivARB(hdc, 1, 100, 2, valid_attrib, values));
   case 11: TRY_ATTRIB(wglGetPixelFormatAttribivARB(hdc, 1, 100, 2, valid_attrib_no_layer, values));
   }
}

int main(int argc, char** argv)
{
    HWND hwnd = create_dummy_window();
    HDC hdc = GetDC(hwnd);
    set_dummy_pixel_format(hdc);
    
    HGLRC ctx = wglCreateContext(hdc);
    wglMakeCurrent(hdc, ctx);
    gladLoadWGL(hdc);
    gladLoadGL();
    
    if (argc >= 2)
    {
        get_attrib(hdc, std::atoi(argv[1]));
        return 0;
    }
    std::cout << "GL_RENDERER: " << glGetString(GL_RENDERER) << std::endl;
    for (int i = 0; i <= 11; i++)
    {
        STARTUPINFOA startup = { .cb = sizeof(STARTUPINFOA) };
        PROCESS_INFORMATION info = { 0 };
        DWORD ret;
        std::string cmd = std::format("{} {}", argv[0], i);
        
        CreateProcessA(NULL, (LPSTR)cmd.c_str(), NULL, NULL, FALSE, 0, NULL, NULL, &startup, &info);
        WaitForSingleObject(info.hProcess, 3000);
        GetExitCodeProcess(info.hProcess, &ret);
        if (ret > 255) std::cout << std::format("Exception 0x{:x}", ret) << std::endl;
        CloseHandle(info.hThread);
        CloseHandle(info.hProcess);
    }
}
